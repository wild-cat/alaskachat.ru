'use strict';

const randomstring = require('randomstring');
const connectionPromise = require('../components/connectionPromise.js');
const logger = require('log4js').getLogger();

var db = null;

function sessionUserByToken(token) {
	return new Promise(function(resolve, reject) {
		connectionPromise().then(function(connection) {
			db = connection;
			var sql = `	SELECT id, 
								nickname, 
								is_friend_from_pikabu,
								is_superadmin
						FROM user 
						JOIN session ON session.user_id = user.id
						WHERE token = '${token}';`;
			//logger.debug(sql);
			return db.queryAsync(sql);
		}).then(function(result) {
			//logger.debug(result);
			if (result.length === 0) {
				reject(new Error(`NO SESSION FOUND FOR TOKEN '${token}'`));
			} else {
				resolve(result[0]);
			}
		});
	});
}

function isSessionExistsByUserId(userId) {
	return new Promise(function(resolve, reject) {
		connectionPromise().then(function(connection) {
			db = connection;
			var sql = `	SELECT count(*) cnt FROM session WHERE user_id = ${userId};`;
			logger.debug(sql);
			return db.queryAsync(sql);
		}).then(function(result) {
			logger.debug(result);
			if (result[0].cnt !== 1) {
				reject(new Error(`NO SESSION FOUND FOR USER '${userId}'`));
			} else {
				resolve();
			}
		});
	});
}

module.exports = {
	sessionStart: function(userId) {
		var token;
		return new Promise(function(resolve, reject) {
			connectionPromise().then(function(connection) {
				db = connection;
				token = randomstring.generate();
				var sql = `	INSERT INTO session(token, user_id, date_created) 
							VALUES ('${token}', ${userId}, NOW());`;
				//logger.debug(sql);
				return db.queryAsync(sql);
			}).then(function(result) {
				//logger.debug(result);
				resolve(token);
			}).catch(function(err) {
				logger.error(err.stack, err.message);
				reject(new Error(`CAN'T START SESSION FOR user_id=${userId}`));
			});
		});
	},

	sessionEnd: function(req, res) {
		return Promise.resolve().then(function() {
			
			if (!req.cookies['AlaskaChatAuthToken']) {
				req.isAuthorized = false;
				throw new Error(`REQUEST '${req.originalUrl}' IS NOT AUTHORIZED`);
			} else {
				return sessionUserByToken(req.cookies['AlaskaChatAuthToken']);
			}

		}).then(function(user) {
			
			req.user_id = user.id;
			return connectionPromise();

		}).then(function(connection) {
			
			var sql = `	DELETE FROM session WHERE user_id = ${req.user_id};`;
			//logger.debug(sql);
			return connection.queryAsync(sql);
		
		}).then(function(result) {

			res.cookie('AlaskaChatAuthToken', '');
			res.redirect(`/`);

		}).catch(function(err) {

			logger.error(err.message, err.stack);

		});
	},

	authorization: function(req) {
		return new Promise(function(resolve, reject) {
			if (!req.cookies['AlaskaChatAuthToken']) {
				req.isAuthorized = false;
				reject(new Error(`REQUEST '${req.originalUrl}' IS NOT AUTHORIZED`));
			} else {
				sessionUserByToken(req.cookies['AlaskaChatAuthToken']).then(function(user) {
					req.isAuthorized = true;
					req.user_id = user.id;
					req.user_nickname = user.nickname;
					req.is_friend_from_pikabu = user.is_friend_from_pikabu;
					req.is_superadmin = user.is_superadmin;
					resolve();
				}).catch(function(err) {
					logger.error(err);
					reject(new Error(`REQUEST '${req.originalUrl}' IS NOT AUTHORIZED`));
				});
			}
		});
	},

	isSessionExistsByUserId: isSessionExistsByUserId
}