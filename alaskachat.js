'use strict';

/*
[2017-12-21 01:22:46.331] [ERROR] [default] - Error: self signed certificate in certificate chain RequestError: Error: self signed certificate in certificate chain
*/
process.env.NODE_TLS_REJECT_UNAUTHORIZED='0'; //todo: check it out whats going on

const express = require('express');
const config = require('./config/common');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const logger = require('log4js').getLogger();
const Promise = require('bluebird');
const moment = require('moment');
const servers = require('./components/servers.js')
const connectionPromise = require('./components/connectionPromise.js')

logger.setLevel(config.logger.level);

// Prettyfing html output with indentation
servers.app.locals.pretty = true;

servers.app.use(bodyParser.json({limit: '5mb'}));
servers.app.use(bodyParser.urlencoded({extended: true, limit: '5mb'}));
servers.app.use(bodyParser.text());

servers.app.use(cookieParser());

servers.app.use(compression());

if (config.app.mode === 'development') {
	servers.app.use(express.static('public'));
}

servers.app.set('views', __dirname + '/view/');
servers.app.set('view engine', 'pug');

servers.app.use('/', require('./routes/site'));

servers.app.all('*', function(req, res, next) {
	res.render('errors/404.pug');
});


/**/
let sockets = {};

function myLog(socket) {
	try {

		console.log('--------- SOCKET LOG ---------');
		console.log('socket.id = ', socket.id);
		
		console.log('my room = ', JSON.stringify(servers.io.sockets.adapter.rooms[socket.chat_id.toString()]));
		console.log('io.sockets.adapter.rooms = ', JSON.stringify(servers.io.sockets.adapter.rooms));	
		console.log('socket.rooms = ', socket.rooms);

		var sockArr = [];
		for (var sockId in servers.io.sockets.connected) {
			if (io.sockets.connected.hasOwnProperty(sockId)) {
				sockArr.push(sockId);
			}
		}

		console.log('io.sockets.connected = ', JSON.stringify(sockArr));
		
		var sockArr = [];
		for (var sockId in sockets) {
			if (sockets.hasOwnProperty(sockId)) {
				sockArr.push(sockId);
			}
			
		}
		console.log('sockets = ', JSON.stringify(sockArr));
		console.log('--------- END LOG ---------');

	} catch (err) {

		logger.error(err.message, err.stack);

	}
}

servers.io.on('connection', function(sock) {
	logger.debug('a client connected');	
	try {
		sockets[sock.id] = sock;
	} catch (err) {
		logger.error(err.message, err.stack);
	}

	sock.on('login', function(user) {
		var db = null;
		var serviceMessageInsertId = null;
			
		try {
			sock.user_id = user.id;
			sock.user_nickname = user.nickname;
			sock.user_avatar = user.avatar;
			sock.user_is_friend_from_pikabu = user.is_friend_from_pikabu;
			sock.chat_id = user.chat_id;
			sock.isAdmin = user.isAdmin;
			sock.is_markall = user.is_markall;

			this.join(user.chat_id.toString());

			logger.debug(`ChatServer: ${user.nickname} joined chat ${user.chat_id}`);	
			
			var users = [];	
			//console.log('io.sockets.adapter.rooms[user.chat_id.toString()]=', io.sockets.adapter.rooms[user.chat_id.toString()]);
			for (var sockId in servers.io.sockets.adapter.rooms[user.chat_id.toString()].sockets) {
				//console.log(sockId + ' is_markall=' + sockets[sockId].is_markall);
				users.push({
					id: sockets[sockId].user_id,
					nickname: sockets[sockId].user_nickname,
					avatar: sockets[sockId].user_avatar,
					is_friend_from_pikabu: sockets[sockId].user_is_friend_from_pikabu,
					is_markall: (sockets[sockId].is_markall) ? true : false,
					isAdmin: sockets[sockId].isAdmin
				})
			}
		} catch (err) {
			logger.error(err.message, err.stack);
		}
		//console.log('users:', users);
		//console.log('io.sockets().clients():', io.sockets().clients());

		//myLog(this);

		try {
			this.emit('login response', users);
			this.to(user.chat_id.toString()).emit('new user joined', user);
		} catch (err) {
			logger.error(err.message, err.stack);
		}

		connectionPromise().then(function(connection) {
		
			db = connection;		
			
			var sql = ` SELECT count(*) cnt 
						FROM message
						WHERE user_id = ${user.id}
						  AND chat_id = ${user.chat_id}
						  AND service_flag = 3;`;

			//logger.debug(sql);

			return db.queryAsync(sql);

		}).then(function(result) {

			var sql = null;

			if (result[0].cnt > 0) {

				sql = `	UPDATE message 
						SET service_flag = 1,
							body = '${user.nickname} joined chat',
							date_created = NOW()
						WHERE user_id = ${user.id}
						  AND chat_id = ${user.chat_id}
						  AND service_flag = 3;` 

			} else {

				sql = `
					INSERT INTO message(body, date_created, date_updated, user_id, chat_id, service_flag)
					VALUES ( 
						CONCAT('${user.nickname}', ' joined chat'),
						NOW(),
						NOW(),
						${user.id},
						${user.chat_id},
						1
					);
				`;

			}

			logger.debug(sql);

			return db.queryAsync(sql);
		
		}).then(function(result) {

			logger.debug(result);

			if (result.insertId && result.insertId != 0) {
				serviceMessageInsertId = result.insertId;
			}
			//logger.debug(result);

		}).catch(function(err) {

			if (serviceMessageInsertId) {
				var sql = `DELETE FROM message WHERE id = ${serviceMessageInsertId};`;
				logger.debug(sql);
				db.queryAsync(sql);
			}

			logger.error(err.message, err.stack);

		});
	});

	sock.on('chat message', function(data) {
		var db = null;
		var _this = this;
		
		data.user_id = this.user_id;

		connectionPromise().then(function(connection) {

			var attach = (data.attach) ? '\'' + data.attach + '\'' : 'NULL';
			var isMarkall = (_this.is_markall) ? 1 : 0;

			var sql = ` INSERT INTO message (body, 
							date_created, 
							date_updated, 
							user_id, 
							chat_id, 
							attach, 
							service_flag,
							is_markall) 
						VALUES (
							'${data.message.replace(/\'/g, '\\\'')}',
							NOW(),
							NOW(),
							${_this.user_id},
							${_this.chat_id},
							${attach},
							0,
							${isMarkall});`

			//logger.debug(sql);

			return connection.queryAsync(sql); 

		}).then(function(result) {

			data['message_id'] = result.insertId;
			_this.to(_this.chat_id.toString()).emit('chat message', data);
			_this.emit('chat message', data);
		
		}).catch(function(err) {

			logger.error(err.message, err.stack);

		});
	});

	sock.on('delete message', function(data) {
		try {
			this.to(this.chat_id.toString()).emit('delete message', data);
		} catch(err) {
			logger.error(err.message, err.stack);
		}

		connectionPromise().then(function(connection) {

			var sql = ` DELETE FROM message
						WHERE id = ${data.message_id}`;

			return connection.queryAsync(sql);

		}).catch(function(err) {

			logger.error(err.message, err.stack);

		})
	});

	sock.on('markall', function(data) {
		var _this = this;

		var isMarkall = null;

		connectionPromise().then(function(connection) {

			isMarkall = (data.is_markall) ? 1 : 0;

			var sql = ` UPDATE message 
						SET is_markall = ${isMarkall}
						WHERE chat_id = ${_this.chat_id}
						  AND user_id = ${data.user_id}`;

			logger.debug(sql);

			return connection.queryAsync(sql);

		}).then(function(result) {

			logger.debug(result);
			
			for (var sock_id in servers.io.sockets.connected) {
				if (servers.io.sockets.connected[sock_id].user_id === data.user_id) {
					servers.io.sockets.connected[sock_id].is_markall = data.is_markall;
					break;
				}
			}
			//console.log('data=', JSON.stringify(data));
			_this.to(_this.chat_id.toString()).emit('markall', data);

		}).catch(function(err) {

			logger.error(err.message, err.stack);

		});
	});

	sock.on('block user', function(data) {
		var user_nickname = null;
		var chat_id = this.chat_id;

		try {

			for (var sockId in servers.io.sockets.adapter.rooms[this.chat_id.toString()].sockets) {
				if (sockets[sockId].user_id === data.user_id) {
					user_nickname = sockets[sockId].user_nickname;
					sockets[sockId].disconnect(true);
					delete sockets[sockId];
				}
			}
		
		} catch (err) {
			logger.error(err.message, err.stack);
		}

		connectionPromise().then(function(connection) {

			var sql = `	UPDATE message 
						SET service_flag = 2,
							body = '${user_nickname} was blocked by chat admin',
							date_created = NOW()
						WHERE user_id = ${data.user_id}
						  AND chat_id = ${chat_id}
						  AND service_flag IN (1,3);`
			logger.debug(sql);

			return connection.queryAsync(sql);				

		}).then(function(result) {

			logger.debug(result);

		}).catch(function(err) {

			logger.error(err.message, err.stack);

		})
	});

	sock.on('disconnect', function() {
		var db = null;
		
		if (!this.user_id) return; // Выход, если клиентский сокет не посылал 'login'

		//myLog(this);

		try {

			this.to(this.chat_id.toString()).emit('user has leaved', this.user_id);
			
			this.leave(this.chat_id.toString());
			
			delete sockets[this.id];
			
			logger.debug(`ChatServer: User '${this.user_nickname}' has leaved '${this.chat_id}'`);
			
		} catch (err) {

			logger.error(err.message, err.stack);

		}	

		//console.log('sock.id=', this.id);
		var _this = this;
		connectionPromise().then(function(connection) {
			db = connection;
			
			var sql = ` UPDATE message 
						SET service_flag = 3,
							date_updated = NOW(),
							body = '${_this.user_nickname} has leaved chat'
						WHERE 	user_id = ${_this.user_id}
						  AND 	chat_id = ${_this.chat_id}
						  AND 	service_flag = 1;`;
		
			logger.debug(sql);

			return db.queryAsync(sql);
				
		}).catch(function(err) {

			logger.error(err.message, err.stack);

		});
	});
});

var serverCheckInterval = setInterval(function() {
	var db = null;

	connectionPromise().then(function(connection) {

		db = connection;

		var sql = `	SELECT
						id,
						DATE_FORMAT(start_time, '%d.%m.%Y %H:%i') start_time
					FROM chat
					WHERE status = 'started' 
					   OR status = 'pending';`;

		return db.queryAsync(sql);

	}).then(function(result) {

		var chatsFinished = [];
		var now = moment();

		result.forEach(function(chat) {
			var startEndTime = moment(chat.start_time, 'DD.MM.YYYY HH:mm');
			startEndTime.hours( startEndTime.hours() + 1 );

			var diff = now.diff(startEndTime, 'seconds');
			if (diff >= 0) {
				chatsFinished.push(chat);
			}
		});

		var promiseChain = Promise.resolve();

		chatsFinished.forEach(function(chat) {
			var isChatEmpty = false;
			promiseChain = promiseChain.then(function() {
				if (!servers.io.sockets.adapter.rooms[chat.id.toString()]) {
					isChatEmpty = true;
				}
				if (!isChatEmpty) {
					for (var sockId in servers.io.sockets.adapter.rooms[chat.id.toString()].sockets) {
						sockets[sockId].emit('chat finished').disconnect(true);
					}
				}
			}).then(function(result) {
				var sql = ` UPDATE chat 
							SET status = 'finished',
								date_updated = NOW();`
				return db.queryAsync(sql);
			}).then(function() {
				var sql = ` DELETE FROM message 
							WHERE chat_id = ${chat.id};`
				return db.queryAsync(sql);
			}).catch(function(err) {
				logger.error(err.message, err.stack);
			});
		});
		
	}).catch(function(err) {

		logger.error(err.message, err.stack);

	});
}, 1000*15);

var serverChatArchivariousInterval = setInterval(function() {
	var db = null;

	connectionPromise().then(function(connection) {

		db = connection;

		var sql = `	SELECT
						id,
						name,
						relative_link,
						DATE_FORMAT(start_time, '%d.%m.%Y %H:%i') start_time,
						DATE_FORMAT(date_created, '%d.%m.%Y %H:%i') date_created,
						DATE_FORMAT(date_updated, '%d.%m.%Y %H:%i') date_updated,
						owner_id,
						first_message
					FROM chat
					WHERE status = 'finished'`;

		return db.queryAsync(sql);

	}).then(function(result) {

		var chatsArchived = [];
		var now = moment();

		result.forEach(function(chat) {
			var startEndTime = moment(chat.start_time, 'DD.MM.YYYY HH:mm');
			startEndTime.hours( startEndTime.hours() + 25 );

			var diff = now.diff(startEndTime, 'seconds');
			if (diff >= 0) {
				chatsArchived.push(chat);
			}
		});

		var promiseChain = Promise.resolve();

		chatsArchived.forEach(function(chat) {
			promiseChain = promiseChain.then(function() {
				var chatFirstMessage = ( chat.first_message ) ? "'" + chat.first_message + "'" : chat.first_message;
				var sql = ` INSERT INTO chat_archive(
								name,
								relative_link,
								start_time,
								date_created,
								date_updated,
								owner_id,
								first_message
							) VALUES (
								'${chat.name}',
								'${chat.relative_link}',
								STR_TO_DATE('${chat.start_time}', '%d.%m.%Y %H:%i'),
								STR_TO_DATE('${chat.date_created}', '%d.%m.%Y %H:%i'),
								NOW(),
								${chat.owner_id},
								${chatFirstMessage}
							)`;
				return db.queryAsync(sql);
			}).then(function() {
				var sql = ` DELETE FROM chat WHERE id = ${chat.id};`;
				return db.queryAsync(sql);
			})
		});

	}).catch(function(err) {

		logger.error(err.message, err.stack);

	});
}, 1000*60*60);

/**/


servers.http.listen(config.app.port, (err) => {
    if (err) {
        logger.error(`Couldn't start server: ${err.message} ${err.stack}`);
        return;
    }
    logger.info(`Server is listening on port ${config.app.port}`);
});



