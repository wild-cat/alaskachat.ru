'use strict';

function loadCurrentProfileInfo() {
	$.ajax({
		method: 'POST',
		url: '/profile/ajax_get_current_profile_info',
		datatype: 'json',
		data: {},
		success: function(result) {
			if (result.status === 'ok') {
				$('.nav-box > .nav > h3').text(result.profile.nickname).show();
			} else {
				console.log('WARN: loadCurrentProfileInfo: Ошибка при загрузке данных профиля. Ошибка сервера.');	
			}
		},
		error: function(result) {
			console.log('WARN: loadCurrentProfileInfo: Ошибка при загрузке данных профиля. Нет соединения с сервером.');
		}
	});
}

function loadChatRoom() {
	$.ajax({
		method: 'POST',
		url: '/chat/ajax_chat_room',
		datatype: 'json',
		data: {},
		success: function(result) {
			if (result.status === 'ok') {
				//$('.nav-box > .nav > h3').text(result.profile.nickname).fadeIn(200);
				//console.log(result);
				var chats = result.chats;
				for (var i = 0; i < chats.length; i++) {
					
					var newElem = null;
					if (chats[i].status === 'started') {
					
						newElem = $('<a href="/chat/' + chats[i].relative_link + '"></a>')
							.append(
								$('<div class="chat-info"></div>')
									.append(
										$('<div></div>')
											.append($('<img src="/images/chat.png"/>'))
											.append($('<h4>' + chats[i].NAME + '</h4>')))
									.append(
										$('<div></div>')
											.append(
												$('<h6>Сообщений </h6>')
													.append('<span>' + chats[i].message_cnt + '</span>'))
											.append($('<h5 class="active-chat">Активный<h5>'))));
					
					} else if (chats[i].status === 'pending') {
						
						var dateComponents = chats[i].start_time.match(/^(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2})$/);
						var startTimeFormatted = '';

						var curDate = (new Date()).getDate();
						curDate = ( curDate < 10 ) ? '0' + curDate : '' + curDate;

						var curMonth = (new Date()).getMonth() + 1;
						curMonth = ( curMonth < 10 ) ? '0' + curMonth : '' + curMonth;

						var curYear = (new Date()).getFullYear() + '';
						
						if (curDate === dateComponents[1] && curMonth === dateComponents[2] && curYear === dateComponents[3]) {
							
							startTimeFormatted = 'в ' + dateComponents[4] + ':' + dateComponents[5];

						} else {
							
							var month = '';
							switch (dateComponents[2]) {
								case '01':
									month = 'января';
									break;
								case '02':
									month = 'февраля';
									break;
								case '03':
									month = 'марта';
									break;
								case '04':
									month = 'апреля';
									break;
								case '05':
									month = 'мая';
									break;
								case '06':
									month = 'июня';
									break;
								case '07':
									month = 'июля';
									break;
								case '08':
									month = 'августа';
									break;
								case '09':
									month = 'сентября';
									break;
								case '10':
									month = 'октября';
									break;
								case '11':
									month = 'ноября';
									break;
								case '12':
									month = 'декабря';
									break;
							}

							startTimeFormatted = dateComponents[1] + ' ' + month + ' в ' + dateComponents[4] + ':' + dateComponents[5];
						}

						
						newElem = $('<a href="/chat/' + chats[i].relative_link + '"></a>')
							.append(
								$('<div class="chat-info"></div>')
									.append(
										$('<div></div>')
											.append($('<img src="/images/chat.png"/>'))
											.append($('<h4>' + chats[i].NAME + '</h4>')))
									.append(
										$('<div></div>')
											.append($('<h5>Начало </h5>')
												.append($('<span>' + startTimeFormatted + '</span>')))));
						/*newElem = $('<a href="/chat/' + chats[i].relative_link + '"></a>')
							.append($('<div class="chat-info"></div>')
								.append(
									$('<div></div>')
										.append($('<img src="/images/chat.png"/>'))
										.append($('<h4>' + chats[i].name + '</h4>')))
									$('<div></div>')
										.append(
											$('<h5>Начало в <h5>')
												.append('<span>' + chatStartTime + '</span>'))*/
					
					}
					
					newElem.appendTo('.chat-note');
					$('.chat-note').fadeIn(400);
				}
			} else {
				console.log('WARN: loadChatRoom: Ошибка при загрузке чатрума. Ошибка сервера.');	
			}
		},
		error: function(result) {
			console.log('WARN: loadChatRoom: Ошибка при загрузке чатрума. Нет соединения с сервером.');
		}
	});	
}

$(document).ready(function() {
	/*if (window.location.href.match(/^(http:\/\/alaskachat.ru\/?|http:\/\/localhost\/?|http:\/\/192.168.1.33\/?)$/)) {
		return;
	}*/
	 $(".btn-chat-menu").click(function(){
		$(".menu-box").animate({width: 'toggle'});
	});
	
	if ($('#is-authorized').val() == true) {
		$('.chat-rules').hide();
		//loadCurrentProfileInfo();
		loadChatRoom();
	} else {
		$('.top-line').hide()
		$('.nav-box').hide()
		$('.chat-rules').show();
	}	
});