'use strict';

var checkInterval = null;
var userLastTypeTime = null;
var socket = null;
var users = {};
var usersCnt = 0;
var myId = null;
var isAdmin = true;
var isChatFinished = false;
var currentAttach = null;

// unread messages
var countUnreadMessages = 0;
var isWindowFocused = true;
var unreadMessagesInterval = null;
var whichTitleTypeNow = 0;

/* HANDLERS */

function handlersChatMain() {
	$.datetimepicker.setLocale('ru');

	$('#chat_start_time').datetimepicker({
		format: 'd.m.Y H:i'
	});

	$('#chat_name').on('keyup', function() {
		if ($(this).val() === '') {
			return;
		}
		$('#chat_relative_link').val(translate($(this).val()).substr(0, 100));
		
		userLastTypeTime = moment();
		if (!checkInterval) {
			startInterval();
		}
	});
	
	$('#chat_relative_link').on('keyup', function() {
		userLastTypeTime = moment();
		if (!checkInterval) {
			startInterval();
		}
	});

	$('.chat-release').on('click', function() {
		
		if (validateChat()) {
			checkLinkAvailability(function(err) {
				if (err) {
					if (!err.message.match(/WARN/)) { 
						$('#error-chat-relative-link').text(err.message).show();
					}
				} else {
					var data = {
						chat_name: $('#chat_name').val(),
						chat_relative_link: $('#chat_relative_link').val(),
						chat_start_time: $('#chat_start_time').val(),
						chat_first_message: $('#chat_first_message').val(),
					};

					var startDate = data.chat_start_time.match(/^(\d{2}\.\d{2})\.\d{4} (\d{2}:\d{2})$/);

					$.ajax({
						url: '/chat',
						method: 'POST',
						data: data,
						dataType: 'json',
						success: function(result) {
							console.log(result);
							if (result.status === 'ok') {
								window.location.href = '/chat/' + result.redirectLink;
								/*if (result.shouldRedirect) {
									//alert('Сейчас Вы будете перенаправлены в чат. Для того, чтобы пригласить в него друзей, скопируйте и отправьте им ссылку http://alaskachat.ru/chat/' + data.chat_relative_link);
									window.location.href = '/chat/' + result.redirectLink;
								} else {
									//alert('Чат "' + data.chat_name + '" начнется ' + startDate[1] + ' в ' + startDate[2] + '. Для того, чтобы пригласить в него друзей, скопируйте и отправьте им ссылку http://alaskachat.ru/chat/' + data.chat_relative_link);	
								}*/
							} else {
								alert('При загрузке данных произошла ошибка');
							}
						},
						error: function() {
							alert('Проверьте соединение с Интернетом');
						}
					});
				}
			});
		}
	});
}

function handlersChatStarted() {
	
	$(window).on('focus', function() {
		isWindowFocused = true;
		countUnreadMessages = 0;
		clearInterval(unreadMessagesInterval);
		unreadMessagesInterval = null;
		whichTitleTypeNow = 0;
		document.title = 'AlaskaChat: ' + $('#chat_name').val();
	});

	$(window).on('blur', function() {
		isWindowFocused = false;
		unreadMessagesInterval = setInterval(function() {
			whichTitleTypeNow = (whichTitleTypeNow + 1) % 2;
			if (countUnreadMessages > 0 && whichTitleTypeNow === 1) {
				document.title = countUnreadMessages + ' новых сообщения(-ний)';
			} else {
				document.title = 'AlaskaChat: ' + $('#chat_name').val();
			}
		}, 2000);
	});

	// Запрещаем F5
	$(document).on('keydown', function(event) {
		if (event.which === 116) {
			event.preventDefault();
		}
	});

	window.addEventListener("beforeunload", function (e) {
		var confirmationMessage = "\o/";

		e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
		return confirmationMessage;              // Gecko, WebKit, Chrome <34
	});

	myId = parseInt($('#user_id').val());
	isAdmin = ( parseInt($('#chat_owner_id').val()) === myId ) ? true : false;
	socket = io();

	socket.emit('login', {
		id: parseInt($('#user_id').val()),
		nickname: $('#user_nickname').val(),
		avatar: $('#user_avatar').val(),
		is_friend_from_pikabu: parseInt($('#user_is_friend_from_pikabu').val()),
		chat_id: $('#chat_id').val(),
		is_markall: ( $('#is_markall').val() === '1' ) ? true : false,
		isAdmin: isAdmin
	});

	socket.on('login response', function(usersInChat) {
		usersInChat.forEach(function(user) {
			users[user.id] = {
				nickname: user.nickname,
				avatar: user.avatar,
				is_friend_from_pikabu: user.is_friend_from_pikabu,
				isAdmin: user.isAdmin,
				is_markall: user.is_markall
			}
		});
		//console.log('users:', users);
		// выводим на страницу всех участников
		for (var user_id in users) {
			if (users.hasOwnProperty(user_id)) {
				var div0 = $('<div id="user-' + user_id + '" class="inchat-user"></div>')
							.append('<img src="' + users[user_id].avatar + '" alt=""/>');
				
				var addLabels = '';
				if (users[user_id].isAdmin) addLabels += '<span class="label-admin"> (Админ)</span>'
				if (users[user_id].is_friend_from_pikabu) addLabels += '<span class="pikabu"> @&nbsp;Друг&nbsp;с&nbsp;Пикабу</span>';
				div0.append('<p>' + users[user_id].nickname + addLabels + '</p>');
				
				if (isAdmin) {
					div0.on('click', function(e) {
						var user_id = parseInt( $(this).attr('id').match(/^user-(\d+)$/)[1] );
						//if (user_id === myId) return;

						$('#user-option-avatar').attr('src', users[user_id].avatar);
						$('#user-option-nickname').text(users[user_id].nickname);
						$('.user-option').attr('process-user', user_id);

						$('.user-option').show();
					});
				}
				
				//Админа вставляем первым
				if (users[user_id].isAdmin) {
					$('.chat-users > h3').after(div0);
				} else {
					$('.chat-users').append(div0);
				}
				usersCnt++;	
			}
		}
		$('#users-in-chat, #users-in-chat-mobile').text(usersCnt);

		newSms({
			user_id: myId,
			type: 1,
			nickname: users[myId].nickname,
			avatar: users[myId].avatar
		});
	});

	socket.on('new user joined', function(user) {
		users[user.id] = {
			nickname: user.nickname,
			avatar: user.avatar,
			is_friend_from_pikabu: user.is_friend_from_pikabu,
			is_markall: user.is_markall,
			isAdmin: user.isAdmin
		};
		
		var div0 = $('<div id="user-' + user.id + '" class="inchat-user"></div>')
				.append('<img src="' + user.avatar + '" alt=""/>');

		var addLabels = '';
		if (user.isAdmin) addLabels += '<span class="label-admin"> (Админ)</span>'
		if (user.is_friend_from_pikabu) addLabels += '<span class="pikabu"> @&nbsp;Друг&nbsp;с&nbsp;Пикабу</span>';
		div0.append('<p>' + user.nickname + addLabels + '</p>');
		
		if (isAdmin) {
			div0.on('click', function() {
				var user_id = parseInt( $(this).attr('id').match(/^user-(\d+)$/)[1] );
				//if (user_id === myId) return;

				$('#user-option-avatar').attr('src', users[user_id].avatar);
				$('#user-option-nickname').text(users[user_id].nickname);
				$('.user-option').attr('process-user', user_id);

				$('.user-option').show();
			});
		}

		//Админа вставляем первым
		if (user.isAdmin) {
			$('.chat-users > h3').after(div0);
		} else {
			$('.chat-users').append(div0);
		}

						
		newSms({
			user_id: user.id,
			type: 1,
			nickname: user.nickname,
			avatar: user.avatar
		});
		usersCnt++;
		$('#users-in-chat, #users-in-chat-mobile').text(usersCnt);
	});

	socket.on('chat message', function(data) {
		newSms({
			message_id: data.message_id,
			user_id: data.user_id,
			type: 0,
			nickname: users[data.user_id].nickname,
			avatar: users[data.user_id].avatar,
			message: data.message,
			attach: data.attach
		});
	});

	socket.on('delete message', function(data) {
		console.log('delete message, data=', data);
		var sms = $('.sms[message_id=' + data.message_id + ']');
		$('img.sms-attach', sms).remove();
		$('p', sms).css('color', '#ccc').text('Сообщение было удалено администратором чата');
	});

	socket.on('markall', function(data) {
		console.log('markall data=', data);
		users[data.user_id]['is_markall'] = !data.is_markall;
		markUserTrigger(data.user_id, true, false); // markall
	});

	socket.on('user has leaved', function(user_id) {
		newSms({
			type: 2,
			nickname: users[user_id].nickname,
			avatar: users[user_id].avatar,
			user_id: user_id
		});
		delete users[user_id];	
		
		$('#user-'+user_id).remove();// Удаление из Списка участников
		
		usersCnt--;
		$('#users-in-chat, #users-in-chat-mobile').text(usersCnt);
	});

	socket.on('chat finished', function() {
		isChatFinished = true;
	});

	socket.on('disconnect', function() {
		if (isChatFinished) {
			alert('Чат завершен');
		} else {
			alert('Потеря соединения с сервером');
		}
	});

	//////

	$('#btn-send-message').on('click', function(){
		var message = $('#message_body').val();
		
		sendMessageHandler(message);
	});

	$('#message_body').on('keyup', function(event) {
		if (event.which == 13) {
			var message = $('#message_body').val();
			
			//console.log('message[message.length-1] === \n', message[message.length-1] === '\n');
			message = message.replace(/\n/g, '');

			sendMessageHandler(message);
		}
	});


	$('#btn-attach-photo').on('click', function() {
		$('#file').click();
	});

	$('#file').on('change', function() {
		if (this.files && this.files[0].size > 1024*512) {
			// 512 kb
			alert('Вложение не может иметь размер более 512 кб');
			return;
		}

		var reader = new FileReader();

		reader.onloadend = function () {
			$('#btn-attach-photo').removeClass('loading-spinner');
			$('#file-preview').attr('src', reader.result);
			currentAttach = reader.result;
			//console.log('reader.result=', reader.result);
		}

		$('#btn-attach-photo').addClass('loading-spinner');
		reader.readAsDataURL(this.files[0]);
	});

	$('#file-preview').on('load', function() {
		$('#btn-attach-photo').hide();
		$(this).show();
	});

	$('#file-preview').on('click', function() {
		removeAttach();
	});

	scrollToBottom();// scroll first time when page loaded
	startCountdown();

	if (isAdmin) {
		$('.sms[service_flag=0]').each(function(index, element) {
			var _this = this;
			$('h5', this).on('click', function() {
				if (confirm('Вы действительно хотите удалить это сообщение? Это действие нельзя будет отменить.')) {
			
					socket.emit('delete message', {
						message_id: parseInt( $(_this).attr('message_id') )
					});
					
					$('img.sms-attach', _this).remove();
					$('p', _this).css('color', '#ccc').text('Сообщение было удалено администратором чата');
					$(this).off('click');
					$(this).remove();
				}
			});
		});
	}

	$('.mark-user').on('click', function() {
		var userId = parseInt( $('.user-option').attr('process-user') );
		markUserTrigger(userId, false); // mark
	});

	$('.markall-user').on('click', function() {
		var userId = parseInt( $('.user-option').attr('process-user') );
		markUserTrigger(userId, true, true); // mark all		
	});

	$('.block-user').on('click', function() {
		var targetUserId = parseInt( $(this).parent().attr('process-user') );

		socket.emit('block user', {
			user_id: targetUserId
		});
	});

	$('.close-box').on('click', function(e) {
		$(this).parent().attr('process-user', '').hide();
		$('#user-option-avatar').attr('src', '/images/photo.jpg');
		$('#user-option-nickname').text('User name');
	});

}

/* VALIDATION */

function validateChat() {
	var chat_name = $('#chat_name').val();
	if ( chat_name === '' || chat_name === ' ') {
		alert('Поле \'Название чата\' не может быть пустым');
		$('#chat_name').focus();
		return false;
	} 
	if (chat_name.length > 100) {
		alert('Поле \'Название чата\' не может быть длиннее 100 символов');
		$('#chat_name').focus();
		return false;
	}

	var chat_relative_link = $('#chat_relative_link').val();
	if ( !chat_relative_link.match(/^[^А-Яа-я?&' 	]+$/) ) {
		alert('Поле \'Ссылка на чат\' НЕ может содержать буквы руссского алфавита, а также символы &, ?, пробелы, табуляцию или быть пустым');
		$('#chat_relative_link').focus();
		return false;
	} 
	if (chat_relative_link.length > 100) {
		alert('Поле \'Ссылка на чат\' не может быть длиннее 100 символов');
		$('#chat_relative_link').focus();
		return false;
	}

	var chat_start_time = $('#chat_start_time').val();
	var chatStartTime = moment(chat_start_time, 'DD.MM.YYYY H:m');
	var now = moment();

	if (chat_start_time && now.diff(chatStartTime) > 0) {
		alert('\'Время старта чата\' не может быть в прошлом');
		$('#chat_start_time').focus();
		return false;
	}	

	var chat_first_message = $('#chat_first_message').val();
	if ( chat_first_message && chat_first_message.length > 1000) {
		alert('Поле \'Первое сообщение в чате\' не может быть длиннее 1000 символов');
		$('#chat_first_message').focus();
		return false;		
	}

	return true;
}

function validateLink() {
	var chat_relative_link = $('#chat_relative_link').val();
	if ( !chat_relative_link.match(/^[^А-Яа-я?&' 	]+$/) ) {
		$('#error-chat-relative-link').text('Поле \'Ссылка на чат\' НЕ может содержать буквы руссского алфавита, а также символы &, ?, пробелы, табуляцию или быть пустым').show();
		
		$('#chat_relative_link').focus();
		return false;
	} 
	if (chat_relative_link.length > 100) {
		$('#error-chat-relative-link').text('Поле \'Ссылка на чат\' не может быть длиннее 100 символов').show();
		$('#chat_relative_link').focus();
		return false;
	}

	$('#error-chat-relative-link').hide();
	return true;
}

/* OTHER */

function startInterval() {
	checkInterval = setInterval(function() {
		var now = moment();
		//console.log(now.diff(userLastTypeTime));
		if (now.diff(userLastTypeTime) > 750) {
			clearInterval(checkInterval);
			checkInterval = null;
			
			if (validateLink()) {
				checkLinkAvailability(function(err) {
					if (!err) {
						$('#error-chat-relative-link').hide();
					} else {
						if (!err.message.match(/WARN/)) { 
							$('#error-chat-relative-link').text(err.message).show();
						}
					}
				});
			}
		}
	}, 200);
}

function translate(phrase) {
	const alphabetRusPlus = {
		'а': 'a',
		'б': 'b',
		'в': 'v',
		'г': 'g',
		'д': 'd',
		'е': 'e',
		'ё': 'yo',
		'ж': 'j',
		'з': 'z',
		'и': 'i',
		'й': 'i',
		'к': 'k',
		'л': 'l',
		'м': 'm',
		'н': 'n',
		'о': 'o',
		'п': 'p',
		'р': 'r',
		'с': 's',
		'т': 't',
		'у': 'u',
		'ф': 'f',
		'х': 'h',
		'ц': 'ts',
		'ч': 'ch',
		'ш': 'sh',
		'щ': 'sch',
		'ъ': '',
		'ы': 'y',
		'ь': '',
		'э': 'e',
		'ю': 'yu',
		'я': 'ya',
		'&': '_',
		'?': '_',
		' ': '_',
		'	': '_',
		'\n': '_'
	};
	const alphabetEng = {
		'a': 'a',
		'b': 'b',
		'c': 'c',
		'd': 'd',
		'e': 'e',
		'f': 'f',
		'g': 'g',
		'h': 'h',
		'i': 'i',
		'j': 'j',
		'k': 'k',
		'l': 'l',
		'm': 'm',
		'n': 'n',
		'o': 'o',
		'p': 'p',
		'q': 'q',
		'r': 'r',
		's': 's',
		't': 't',
		'u': 'u',
		'v': 'v',
		'w': 'w',
		'x': 'x',
		'y': 'y',
		'z': 'z'
	};


	var source = phrase.split('');
	var dest = [];
	
	for (var i = 0; i < source.length; i++) {
		var ind = source[i].toLowerCase();
		if (alphabetEng[ind]) {
			dest.push(source[i].toLowerCase());
		} else if (alphabetRusPlus[ind] !== undefined) {
			dest.push(alphabetRusPlus[ind]);
		} else {
			dest.push(source[i]);
		}
	}

	return dest.join('');
}

function checkLinkAvailability(callback) {
	$.ajax({
		method: 'POST',
		url: '/chat/isChatRelativeLinkAvailable',
		data: {
			chat_relative_link: $('#chat_relative_link').val()
		},
		success: function(result) {
			if (result.status === 'ok') {
				callback(); 
			} else {
				callback(new Error('Чат с такой ссылкой уже существует'));
				
			}
		},
		error: function(result) {
			callback(new Error('WARN: Нет соединения с Интернетом'));
		}
	});
}

function scrollToBottom() {
	var mb = $('.message-box').get(0);
	mb.scrollTop = mb.scrollHeight;
}

function newSms(data) {
	console.log('newsms data=', data);
	if (isChatFinished) return;

	var sent = ( data.user_id === myId ) ? ' sent' : '';
	var addtext = '';
	switch (data.type) {
		case 1:
			addtext = ' присоединился к чату';
			break;
		case 2:
			addtext = ' покинул чат';
			break;
	}
	var addMessageId = (data.message_id) ? ' message_id="' + data.message_id + '"' : '';

	var div0 = $('<div class="sms' + sent + '" user_id="' + data.user_id + '"' + addMessageId + '></div>')
				.append($('<img src="' + data.avatar + '"/>'));
	
	var addClass = '';
	if ( users[data.user_id]['is_markall'] ) addClass += ' markall';
	if ( users[data.user_id]['is_marked'] ) addClass += ' marked';
	var div1 = $('<div class="' + addClass + '"></div>')
				.append($('<h3>' + data.nickname + addtext + '</h3>'));
	
	if (isAdmin && data.type === 0) {
		div1.append(
			$('<h5></h5>').on('click', function() {
				if (confirm('Вы действительно хотите удалить это сообщение? Это действие нельзя будет отменить.')) {
					
					socket.emit('delete message', {
						message_id: parseInt( $(this).parent().parent().attr('message_id') )
					});
					
					var p = $(this).next().next();
					var attach = p.next();
					
					p.css('color', '#ccc').text('Сообщение было удалено администратором чата');

					if (attach.length) attach.remove();

					$(this).off('click');
					$(this).remove();
				}
			}));
	}

	div1.append($('<h6>' + moment().format('HH:mm') + '</h6>'));
	
	if (data.type === 0) {
		// 0 - обычное сообщение
		div1.append($('<p classs="received">').text(data.message));
	}

	if (data.attach) {
		div1.append($('<img class="sms-attach" src="' + data.attach +'"/>'))
	}

	div1.appendTo(div0);

	$('.message-box').append(div0);

	scrollToBottom();

	if (!isWindowFocused && data.type === 0) {
		countUnreadMessages++;
	}
}

function calculateTimeComponents(obj) {
	var time = obj.startMoment;
	var now = moment();

	var minutes = 0;
	var seconds = 0;
	var t = time.diff(now, 'seconds');
	if (t >= 60) {
		minutes = Math.floor(t / 60);
		seconds = t % 60;
	} else {
		minutes = 0;
		seconds = t;
	}
	var mm = minutes + '';
	var ss = seconds + '';
	if (minutes < 10) mm = '0' + mm;
	if (seconds < 10) ss = '0' + ss;

	//console.log(mm, ss);

	return {
		minutes: minutes,
		seconds: seconds,
		mm: mm,
		ss: ss
	}	
}

function markUserTrigger(userId, isAll, isInitiator) {
	var action = null;
	var key = (isAll) ? 'is_markall' : 'is_marked';
	//var userId = ( isInitiator ) ? parseInt( $('.user-option').attr('process-user') ) : myId;


	if ( !users[userId][key] ) {
		users[userId][key] = true;
		action = 1;
	} else {
		users[userId][key] = false;
		action = 2;
	}

	var markClass = (isAll) ? 'markall' : 'marked'

	$('.sms[user_id=' + userId + ']').each(function(index, elem) {
		var deepElement = $('div', elem).eq(0);
		
		switch (action) {
			case 1: 
				deepElement.addClass(markClass);
				break;
			case 2:
				deepElement.removeClass(markClass);
				break;
		}
	});

	if (isAll && isInitiator) {
		socket.emit('markall', {
			user_id: userId,
			is_markall: users[userId]['is_markall']
		});
	}
}

function sendMessageHandler(message) {
	if (message.match(/^s*$/) && !currentAttach) return;
	if (message.length > 1000) {
		alert('Длина сообщения не может превышать 1000 символов');
		return false;
	}

	socket.emit('chat message', {
		message: message,
		attach: currentAttach
	});

	// на этом этапе мы не знаем какой идентификатор будет у сообещния в бд
	// поэтому не сможем реализовать удаление этого сообщения в реальном времени
	/*newSms({
		user_id: myId,
		type: 0,
		nickname: users[myId].nickname,
		avatar: users[myId].avatar,
		message: message,
		attach: currentAttach
	});*/

	$('#message_body').val('');
	removeAttach();
}

function removeAttach() {
	if (currentAttach) {
		$('#file').files = undefined;
		currentAttach = null;

		$('#file-preview').hide();
		$('#btn-attach-photo').show();
	}
}

var interval = null;
var startMoment = null;
var isWarningFired = false;

function startCountdown() {
	startMoment = moment( $('#chat_start_time').val(), 'DD.MM.YYYY HH:mm' );
	startMoment.hours( startMoment.hours() + 1 );

	var obj = calculateTimeComponents({ startMoment: startMoment });
	$('#time-left, #time-left-mobile').text(obj.mm + ':' + obj.ss);

	interval = setInterval(function() {
		
		var obj = calculateTimeComponents({ startMoment: startMoment });
		$('#time-left, #time-left-mobile').text(obj.mm + ':' + obj.ss);
		
		if (!isWarningFired && obj.minutes <= 9) {
			// Осталось 9 минут + 60 секунд
			alert('Чат завершится через 10 минут');
			isWarningFired = true;
		}

		if (obj.minutes === 0 && obj.seconds <= 0) {
			clearInterval(interval);
			console.log('Таймер остановлен');
		}
	}, 1000);
}


$(document).ready(function() {
	if (window.location.href.match(/chat\/create/)) {
		handlersChatMain();	
	} else {
		handlersChatStarted();
	}
});