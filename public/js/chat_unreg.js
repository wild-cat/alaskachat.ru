'use strict';

$(document).ready(function() {
	
	$('.email-left').on('click', function() { 
		
		if (!$('.user-mail.main-field').val().match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
			alert('Поле \'email\' заполнено неправильно');
			$('.user-mail.main-field').focus();
			return;
		}

		$.ajax({
			method: 'POST',
			url: '/chat/ajax_subscribe',
			data: {
				user_email: $('.user-mail.main-field').val(),
				chat_id: $('#chat_id').val()
			},
			success: function(result) {
				if (result.status === 'ok') {
					alert('Напоминание успешно создано');
				} else {
					console.log('WARN: ' + result.error);
					if (result.error === 'Email exists') {
						alert('Ошибка: кто-то уже указал данный email')
					}
				}
			},
			error: function(error) {
				console.log('WARN: Нет соединения с сервером' );
			}
		});
	});

	var cb = new ClipboardJS('.chat-link');

	cb.on('success', function(e) {
		e.clearSelection();
		console.info('Ссылка на чат скопирована:', e.text);
		
		$('#success-copy').fadeIn(200);
		var timeout = setTimeout(function() {
			$('#success-copy').fadeOut(200);
		}, 3000);
		
	});
});