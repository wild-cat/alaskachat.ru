'use strict';

var refreshStatisticsInterval = null;

function statisticsHandler() {
	refreshStatisticsInterval = setInterval(function() {
		//loadStatUsers();
		loadStatSockets();
	}, 1000 * 20);

	loadStatUsers();
	loadStatSockets();
}

function loadStatUsers() {
	
	$.ajax({
		method: 'GET',
		url: '/stat/ajax_stat_users',
		data: {},
		success: function(result) {
			if (result.status === 'ok') {
				//console.log(result.data);

				$('#users_total').text(result.data.users_total);
				$('#users_online').text(result.data.users_online);
				$('#new_users_last_day').text(result.data.new_users_last_day);
				$('#new_users_last_week').text(result.data.new_users_last_week);
				$('#new_users_last_two_weeks').text(result.data.new_users_last_two_weeks);

				$('#indicator-users').fadeIn(200);
				setTimeout(function() {
					$('#indicator-users').fadeOut(200);
				}, 5000);
			} else {
				console.log(result.message);
			}
		},
		error: function(error) {
			console.log('WARN: Нет соединения с сервером');
		}
	});
}

function loadStatSockets() {
	
	$.ajax({
		method: 'GET',
		url: '/stat/ajax_stat_sockets',
		data: {},
		success: function(result) {
			if (result.status === 'ok') {
				//console.log(result.data);

				$('#count-rooms-real').text(result.countRoomsReal);
				$('#count-sockets-connected').text(result.countSocketsConnected);
				
				$('#indicator-sockets').fadeIn(200);
				setTimeout(function() {
					$('#indicator-sockets').fadeOut(200);
				}, 5000);
			} else {
				console.log(result.message);
			}
		},
		error: function(error) {
			console.log('WARN: Нет соединения с сервером');
		}
	});
}

$(document).ready(function() {
	statisticsHandler();
});