'use strict';

function handlersProfile() {
	
	$('.user-save').on('click', function() {
		var form = new FormData();

		var files = $('#file-avatar').get(0).files;
		if ( files.length ) {
			if (files[0].size > 10485760 / 20) {
				//512kb
				alert('Загружаемый файл должен иметь размер не более 512 kb');
				return;
			}
			form.append('upload', files[0], files[0].name);
		}

		var user_nickname = $('#user_nickname').val();
		if (user_nickname.match(/^\s*$/)) {
			alert('Поле \'Ник\' не может быть пустым');
			return;
		}
		if (user_nickname.length > 30) {
			alert('Поле \'Ник\' не может быть длиннее 30 символов');
			return;
		}

		form.append('nickname', user_nickname);

		$.ajax({
			method: 'POST',
			url: '/profile/',
			processData: false,
			contentType: false,
			data: form,
			success: function(result) {
				if (result.status === 'ok') {
					if (result.avatarHref) {
						$('#avatar-icon').attr('src', result.avatarHref);
					}
					
					//console.log('chat_relative_link=', $('#chat-relative-link').val());
					if ( $('#chat-relative-link').val() ) {
						//alert('Профиль успешно сохранен. Сейчас Вы будете перенаправлены в чат.');
						window.location.href = '/chat/' + $('#chat-relative-link').val();
					} else if ( parseInt($('#is-first-time').val()) ) {
						alert('Профиль успешно сохранен');
						window.location.href = '/chat/create';
					} else {
						alert('Профиль успешно сохранен');
					}
				} else {
					alert('Ошибка при сохранении профиля');
				}
			},
			error: function(error) {
				alert('Ошибка интернет-соединения');
			}
		});
	});

	$('.avatar-container').on('click', function() {
		$('#file-avatar').click();
	});

	$('#file-avatar').on('change', function() {
		if (this.files.length) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#avatar-icon').attr('src', e.target.result);
			};

			reader.readAsDataURL(this.files[0]);
		}
	});
}

function handlersProfileFriendFromPikabu() {

	$('.user-save').on('click', function() {
		if ( $('.main-field-pass').val().match(/^s*$/) ) {
			alert('Поле \'Код\' не может быть пустым');
			$('.main-field-pass').focus();
			return;
		}

		$.ajax({
			method: 'POST',
			url: '/profile/friend_from_pikabu',
			data: {
				code: $('.main-field-pass').val()
			},
			success: function(result) {
				if (result.status === 'ok') {
					alert('Поздравляем! Код активирован.');
					window.location.href = '/chat/create';
				} else {
					alert('Увы! Неверный код.');
				}
			},
			error: function(error) {
				console.error('Сервер недоступен');
			}
		})
	});

}

$(document).ready(function() {
	if (window.location.href.match(/friend_from_pikabu/)) {
		handlersProfileFriendFromPikabu();
	} else {
		handlersProfile();
	}
});