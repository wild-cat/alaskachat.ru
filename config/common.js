const HOST = 'localhost';
const PORT = 80;

module.exports = {
	app: {
		mode: 'development',
		host: HOST,
		port: PORT,
		emailAdmin: 'kirillmybox@rambler.ru',
		admin: 'Vladykin Kirill'
	},
	database: {
		host: '127.0.0.1',
		port: 3306,
		user: 'root',
		password: '',
		database: 'alaskachat'
	},
	logger: {
		level: 'DEBUG'
	},
	vkApp: {
		id: 6368787,
		name: 'AlaskaChat',
		clientSecret: 'aNELoehzNfi8uIukp2Gf',
		redirectUrl: 'http://' + HOST + ':' + PORT + '/auth/login_vk_callback',
		version: 5.69
	},
	mailer: { 
		smtpConfig: {
			host: 'smtp.yandex.ru',
			port: 465,
			secure: true, // true for 465, false for other ports 587
			auth: {
				user: '',
				pass: ''
			}
			/*service: "gmail",
			auth: {
				user: 'mortal.fighter.89@gmail.com',
				pass: 'CucumbeR_7386px'
			}*/
		}
	}
};