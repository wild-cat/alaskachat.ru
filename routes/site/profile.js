'use strict';

const router = require('express').Router();
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');
const sizeOfAsync = Promise.promisify(require('image-size'));

const connectionPromise = require('../../components/connectionPromise.js');
const logger = require('../../components/myLogger.js');


const PROMO_CODE_PIKABU = 'УБАКИП';

const multer  = require('multer');
//todo: fix bug with duplicating && corrupting images 

const storage = multer.diskStorage({
	
	destination: function (req, file, cb) {
		
		fs.statAsync(path.normalize(__dirname + '/../../public/images/uploads/user_'+req.user_id)).then(function() {
			
			cb(null, 'public/images/uploads/user_'+req.user_id);	
		
		}).catch(function(err) {
			
			fs.statAsync(path.normalize(__dirname + '/../../public/images/uploads'))
			
			.catch(function(err) {
				
				return fs.mkdirAsync(path.normalize(__dirname + '/../../public/images/uploads'), 0o755);

			})
			
			.then(function() {
				
				fs.mkdirAsync(path.normalize(__dirname + '/../../public/images/uploads/user_'+req.user_id), 0o755) 
			
			})
			
			.then(function() {
			
				cb(null, 'public/images/uploads/user_'+req.user_id);
			
			}).catch(function(err) {
			
				logger.error(req, err.stack, err.message);
			
			});
		});	
	},
	
	filename: function (req, file, cb) {
		const i = file.originalname.lastIndexOf('.');
		const ext = file.originalname.substr(i);
		cb(null, Date.now() + ext);
	}
});

const uploader = multer({ 
	storage: storage,
	fileFilter: function (req, file, cb) {
		if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png' && file.mimetype !== 'image/gif') {
			cb(null, false);
			return;	
		}
		cb(null, true);
	}
});

router.get('/friend_from_pikabu', function(req, res, next) {

	if (!req.isAuthorized) {
		res.redirect(`/?message='Пожалуйста, авторизуйтесь в системе`);
		return;
	}

	Promise.resolve().then(function() {

		res.render('site/profile_pikabu.pug', {
			isAuthorized: (req.isAuthorized) ? '1' : '0',
			user_nickname: req.user_nickname,
			is_friend_from_pikabu: req.is_friend_from_pikabu,
			is_superadmin: req.is_superadmin
		});
		
	}).catch(function(err) {

		logger.error(req, err.message, err.stack);
		res.render('errors/500.pug');

	});
});

router.get('/:greetings(\\S+)?', function(req, res, next) {
	var db = null;

	if (!req.isAuthorized) {
		res.redirect(`/?message='Пожалуйста, авторизуйтесь в системе`);
		return;
	}

	connectionPromise().then(function(connection) {
	
		db = connection;

		var sql = `
			SELECT 
				user_id,
				user_nickname,
				user_avatar
			FROM v_user
			WHERE user_id = ${req.user_id};
		`;
		logger.debug(req, sql);
		return db.queryAsync(sql);
	
	}).then(function(result) {

		logger.debug(req, result);

		var chat_relative_link = null;

		if (req.cookies['AlaskaChatRelativeLink']) {
			chat_relative_link = req.cookies['AlaskaChatRelativeLink'];
			res.cookie('AlaskaChatRelativeLink', '');
		}

		res.render('site/profile.pug', {
			data: result[0],
			isFirstTime: req.params.greetings === 'greetings',
			chat_relative_link: ( chat_relative_link ) ? chat_relative_link : '',
			isAuthorized: (req.isAuthorized) ? '1' : '0',
			user_nickname: req.user_nickname,
			is_friend_from_pikabu: req.is_friend_from_pikabu,
			is_superadmin: req.is_superadmin
		});

	}).catch(function(error) {

		logger.error(req, error.message, error.stack);
		res.render('errors/500.pug');

	});
});

router.post('/friend_from_pikabu', function(req, res, next) {
	var db = null;

	connectionPromise().then(function(connection) {

		db = connection;

		if (req.body.code.toUpperCase() !== PROMO_CODE_PIKABU) {
			res.json({
				status: 'not ok'
			});
			throw new Error('exit');
		}

		var sql = ` UPDATE user 
					SET is_friend_from_pikabu = 1
					WHERE id = ${req.user_id};`;

		logger.debug(req, sql);

		return db.queryAsync(sql);

	}).then(function(result) {

		logger.debug(req, result);

		res.json({
			status: 'ok'
		});
		
	}).catch(function(err) {
		if (err.message === 'exit') return;

		logger.error(req, err.message, err.stack);
		res.render('errors/500.pug');

	});
});

/*router.post('/ajax_get_current_profile_info', function(req, res, next) {
	if (!req.isAuthorized) {
		res.redirect(`/?message='Пожалуйста, авторизуйтесь в системе`);
		return;
	}

	connectionPromise().then(function(connection) {

		var sql = `
			SELECT id, nickname FROM user WHERE id = ${req.user_id};
		`;

		return connection.queryAsync(sql);

	}).then(function(result) {

		res.json({
			status: 'ok',
			profile: result[0]
		});

	}).catch(function(err) {
		
		logger.error(req, err.message, err.stack);
		res.json({
			status: 'not ok'
		});

	})
});*/

router.post('/', uploader.single('upload'), function(req, res, next) {
	function convertResourceLocator(path) {
		var uri = path.replace(/\\/g, '/');
		var ind = uri.indexOf('/');
		var res = uri.substr(ind);
		return res;
	}

	var db = null;
	var isAvatar = false;
	var avatar = null;

	Promise.resolve().then(function() {
	
		if (!req.isAuthorized) {
			throw new Error('USER_IS_NOT_AUTHORIZED');
		}

		if (!req.body.nickname) {
			throw new Error(`Parameters validation error (req.body.nickname): '${req.body.nickname}'.`);
		}
		if (!req.body.nickname.length > 30) {
			throw new Error(`Parameters validation error: req.body.nickname.length > 30`);	
		}

		return connectionPromise();

	}).then(function(connection) {
	
		db = connection;

		var nickname = '\'' + req.body.nickname.replace(/\'/g, '\\\'') + '\'';
		isAvatar = ( req.file ) ? true : false;

		if (isAvatar) {
			avatar = convertResourceLocator(req.file.path);
		}

		var sql = `
			UPDATE user
			SET nickname = ${nickname}`;
		
		if (isAvatar) {
			sql += `\n, avatar = '${avatar.replace(/\'/g, '\\\'')}'`;
		}
		
		sql += `\nWHERE id = ${req.user_id};`;
		
		logger.debug(req, sql);

		return db.queryAsync(sql);

	}).then(function(result) {

		logger.debug(req, result);

		res.json({
			status: 'ok',
			avatarHref: avatar
		});
		

	}).catch(function(error) {

		logger.error(req, error.message, error.stack);
		
		res.json({
			status: 'not ok'
		});

	});
});

module.exports = router;