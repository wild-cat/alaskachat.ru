'use strict';

const router = require('express').Router();

const Promise = require('bluebird');
const rp = require('request-promise');
const fs = require('fs');
const path = require('path');
const logger = require('log4js').getLogger();
const imageDownloader = require('image-downloader');

const config = require('../../config/common.js');
const connectionPromise = require('../../components/connectionPromise.js');
const auth = require('../../components/auth.js');

router.get('/login_soc', function(req, res, next) {
	res.redirect(`https://oauth.vk.com/authorize?client_id=${config.vkApp.id}&display=page&redirect_uri=${config.vkApp.redirectUrl}&response_type=code&v=${config.vkApp.version}`);
});

router.get('/login_soc_and_enter/:chatRelativeLink(\\S+)', function(req, res, next) {
	//logger.debug('/login_soc_and_enter/' + req.params.chatRelativeLink);
	Promise.resolve().then(function() {
		
		if (!req.params.chatRelativeLink.match(/^[^А-Яа-я?&' 	]+$/)) {
			throw new Error(`Parameters validation error (chatRelativeLink): '${req.params.chatRelativeLink}'.`);
		}

		res.cookie('AlaskaChatRelativeLink', req.params.chatRelativeLink);
		res.redirect(`https://oauth.vk.com/authorize?client_id=${config.vkApp.id}&display=page&redirect_uri=${config.vkApp.redirectUrl}&response_type=code&v=${config.vkApp.version}`);

	}).catch(function(err) {

		 if (err.message.match(/^(Warn|Parameters validation)/)) {
			logger.debug(err.message);
			res.render('errors/404.pug');
		} else {
			logger.error(err.message, err.stack);
			res.render('errors/500.pug');
		}
	});
});

router.get('/logout', function(req, res, next) {
	auth.sessionEnd(req, res).catch(function(err) {
		logger.error(err);
		res.render('errors/500.pug');
	});
});

router.get('/login_vk_callback', function(req, res, next) {
	var db = null;
	var access = null;
	var user = null;
	var university = null;
	var newUserId = null; 
	var avatarPath = null;
	var avatarHref = null;
	var isAvatarFileCreated = false;
	var userHasPhoto = false;
	var userId = null;

	Promise.resolve().then(function() {
		
		//logger.debug(req.query.code);
		var options = {
			uri: 'https://oauth.vk.com/access_token',
			qs: {
				client_id: config.vkApp.id,
				code: req.query.code,
				client_secret: config.vkApp.clientSecret,
				redirect_uri: config.vkApp.redirectUrl
			},
			json: true
		};

		return rp(options);

	}).then(function(result) {
		
		logger.debug(result);
		access = result;
		return connectionPromise();
	
	}).then(function(connection) {
		
		db = connection;
		var sql = `	INSERT INTO access_token(access_token, expires_in, vk_id, date_created)
					VALUES (
						'${access.access_token}',
						${access.expires_in},
						${access.user_id},
						NOW())`;
		logger.debug(sql);
		return db.queryAsync(sql);
	
	}).then(function(result) {
	
		logger.debug(result);
		var options = {
			uri: 'https://api.vk.com/method/users.get',
			qs: {
				access_token: access.access_token,
				user_ids: access.user_id,
				fields: 'photo_200',
				v: config.vkApp.version
			},
			json: true
		};
		logger.debug(options);
		return rp(options);
	
	}).then(function(result) {

		logger.debug(JSON.stringify(result));
		user = result.response[0];
		
		var sql = `SELECT id FROM \`user\` WHERE vk_id = ${user.id};`;

		logger.debug(sql);
		return db.queryAsync(sql);
	
	}).then(function(result) {
	
		logger.debug('USER' + JSON.stringify(result));

		if (result.length) {
			userId = result[0].id;
		}

		if (userId) {
			
			// EXISTING USER
			auth.sessionStart(userId).then(function(token) {
				res.cookie('AlaskaChatAuthToken', token);
				//logger.debug('COOKIE AlaskaChatRelativeLink=', req.cookies['AlaskaChatRelativeLink']);
				if (req.cookies['AlaskaChatRelativeLink']) {
					//logger.debug('v1');
					var chatRelativeLink = req.cookies['AlaskaChatRelativeLink'];
					res.cookie('AlaskaChatRelativeLink', '');
					res.redirect('/chat/' + chatRelativeLink);
				} else {
					//logger.debug('v2');
					res.redirect(`/chat/create`);
				}	
			}).catch(function(err) {
				logger.error(err);
				res.render('errors/500.pug');
			});
			
		
		} else {
			
			// NEW USER
		
			return Promise.resolve().then(function() {

				var user_nickname = user.first_name.replace(/\'/g, '\\\'') + ' ' + user.last_name.replace(/\'/g, '\\\'');

				var sql = `	INSERT INTO \`user\`(	first_name, 
													last_name, 
													avatar,
													nickname,
													vk_id, 
													date_register)
							VALUES (	'${user.first_name.replace(/\'/g, '\\\'')}',
										'${user.last_name.replace(/\'/g, '\\\'')}',
										'/images/photo.jpg',
										'${user_nickname}',
										${user.id},
										NOW())`;
				logger.debug(sql);
				return db.queryAsync(sql);
			
			}).then(function(result) {
				
				logger.debug(result);
				newUserId = result.insertId;
				//auth.sessionStart(newUserId);

				userHasPhoto = (user.photo_200 !== 'https://vk.com/images/camera_200.png') ? true : false;		
				
				var promiseChain = Promise.resolve();
				if (userHasPhoto) {
					
					// create user folder, download photo from vk, save it and update mysql
					promiseChain = promiseChain.then(function() {
						fs.mkdirSync(path.normalize(__dirname + '/../../public/images/uploads/user_'+newUserId), 0o755);
					}).then(function() {
						var ext = user.photo_200.substr(user.photo_200.lastIndexOf('.') + 1);
						var time = (new Date()).getTime();
						avatarPath = path.normalize(`${__dirname}/../../public/images/uploads/user_${newUserId}/avatar_${time}.${ext}`);
						avatarHref = `/images/uploads/user_${newUserId}/avatar_${time}.${ext}`;
						
						const options = {
							url: user.photo_200,
							dest: avatarPath                 
						}
						return imageDownloader.image(options);
					}).then(function(res) {
						isAvatarFileCreated = true;
						//update db
						var sql = `UPDATE \`user\` SET avatar = '${avatarHref}' WHERE id = ${newUserId};`;
						logger.debug(sql);
						return db.queryAsync(sql);
					}).catch(function(err) {
						logger.error(err.message+' '+err.stack);
					});
				}
				return promiseChain;
			
			}).then(function(result) {
				auth.sessionStart(newUserId).then(function(token) {
					//logger.debug('v3');
					//logger.debug('COOKIE AlaskaChatRelativeLink=', req.cookies['AlaskaChatRelativeLink']);
					res.cookie('AlaskaChatAuthToken', token);
					res.redirect(`/profile/greetings`);	
				}).catch(function(err) {
					logger.error(err);
					res.render('errors/500.pug');
				});
			
			}).catch(function(err) {
			
				logger.error(err.message, err.stack);
				
				if (isAvatarFileCreated) {
					logger.debug('REMOVE AVATAR FROM DISK...');
					fs.unlinkSync(avatarPath);
				}

				if (newUserId) {
					logger.debug('ROLLBACK User...');
					var sql = `DELETE FROM \`user\` WHERE id = ${newUserId};`;
					logger.debug(sql);
					db.queryAsync(sql);
					/*logger.debug(`END SESSION FOR USER ${newUserId}...`);
					auth.sessionEnd(newUserId);*/
				}

				if (access) {
					logger.debug('ROLLBACK access_token...');
					var sql = `DELETE FROM access_token WHERE access_token = '${access.access_token}';`;
					logger.debug(sql);
					db.queryAsync(sql);
				}

				res.render('errors/500.pug');
			});

		}

	}).catch(function(err) {
	
		logger.error(err.message, err.stack);
		
		//todo: remove both on server and client
		/*logger.debug(`END SESSION FOR USER ${userId}...`);
		auth.sessionEnd(userId);*/

		if (access) {
			logger.debug('ROLLBACK access_token...');
			var sql = `DELETE FROM access_token WHERE access_token = '${access.access_token}';`;
			logger.debug(sql);
			db.queryAsync(sql);
		}

		res.render('errors/500.pug');
	
	});
});

module.exports = router;