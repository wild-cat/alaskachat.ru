'use strict';

const router = require('express').Router();
const Promise = require('bluebird');
const moment = require('moment');

const connectionPromise = require('../../components/connectionPromise.js');
const util = require('../../components/myUtil.js');
const logger = require('../../components/myLogger.js');

function validateChat(chat) {
	if (!chat.chat_name) {
		throw new Error(`Parameters validation error (chat.chat_name): '${chat.chat_name}'.`);
	} else if (chat.chat_name.length > 100) {
		throw new Error(`Parameters validation error: chat.chat_name.length = ${chat.chat_name.length}.`);
	}

	if (!chat.chat_relative_link.match(/^[^А-Яа-я?&' 	]+$/)) {
		throw new Error(`Parameters validation error (chat.chat_relative_link): '${chat.chat_relative_link}'.`);
	}

	if (chat.chat_start_time && !chat.chat_start_time.match(/^\d\d.\d\d.\d\d\d\d \d\d:\d\d$/)) {
		throw new Error(`Parameters validation error (chat.chat_start_time): '${chat.chat_start_time}'.`);	
	}

	var chatStartTime = moment(chat.chat_start_time, 'DD.MM.YYYY H:m');
	var now = moment();

	if (chat.chat_start_time && now.diff(chatStartTime) > 0) {
		throw new Error(`Parameters validation error (chat.chat_start_time): in the past.`);	
	}	

	if (chat.chat_first_message && chat.chat_first_message.length > 1000) {
		throw new Error(`Parameters validation error: chat.chat_first_message.length = ${chat.chat_first_message.length}.`);
	}
}


router.get('/create', function(req, res, next) {
	
	if (!req.isAuthorized) {
		res.redirect('/?message=Пожалуйста авторизуйтесь на сайте');
		return;
	}

	connectionPromise().then(function(connection) {

		var sql = `
			SELECT 
				user_id,
				user_nickname
			FROM v_user
			WHERE user_id = ${req.user_id};
		`;

		logger.debug(req, sql);
		return connection.queryAsync(sql);

	}).then(function(result) {
		
		logger.debug(req, result);	
		res.render('site/chat_main.pug', {
			data: result[0],
			isAuthorized: (req.isAuthorized) ? '1' : '0',
			user_nickname: req.user_nickname,
			is_friend_from_pikabu: req.is_friend_from_pikabu,
			is_superadmin: req.is_superadmin
		});

	}).catch(function(err) {

		logger.error(req, err.message, err.stack);
		res.render('errors/500.pug');
	
	});
});


router.get('/:chatRelativeLink(\\S+)', function(req, res, next) {
	var db = null;
	var messages = [];
	var chat = null;
	var chatInfo = null;
	var serviceMessageInsertId = null;

	connectionPromise().then(function(connection) {

		db = connection;

		if (!req.params.chatRelativeLink.match(/^[^А-Яа-я?&' 	]+$/)) {
			throw new Error(`Parameters validation error (chatRelativeLink): '${req.params.chatRelativeLink}'.`);
		}

		var sql = `
			SELECT 
				id,
				name,
				owner_id,
				relative_link,
				DATE_FORMAT(start_time, '%d.%m.%Y %H:%i') start_time
			FROM chat
			WHERE relative_link = '${req.params.chatRelativeLink}';
		`;

		logger.debug(req, sql);

		return db.queryAsync(sql);

	}).then(function(result) {

		logger.debug(req, result)
		chat = result[0];

		if (!result.length) {
			throw new Error(`Warn: chat with link '${req.body.chatRelativeLink}' is not exist`);
			return;
		}

		var now = moment();
		var startTime = moment(chat.start_time, 'DD.MM.YYYY HH:mm');
		var diff = now.diff(startTime, 'seconds');
		//console.log('now=', now, ',chat.start_time=', chat.start_time, ', startTime=', startTime, ', diff=', diff);
		if (diff < 0) {
			chatIsPending(req, res, next, chat, db);
		} else if (diff < 3600) {
			chatStarted(req, res, next, chat, db);
		} else {
			chatFinished(req, res, next, chat, db);
		}

	}).catch(function(err) {

		 if (err.message.match(/^Warn/)) {
		 	logger.debug(req, err.message);
		 	res.render('errors/404.pug');
		 } else if (err.message.match(/^Parameters validation/)) {
			logger.debug(req, err.message);
			res.render('errors/404.pug');
		} else {
			logger.error(req, err.message, err.stack);
			res.render('errors/500.pug');
		}
	});
});


function chatIsPending(req, res, next, chat, db) {
	//console.log('CHAT IS PENDING');

	Promise.resolve().then(function() {

		var sql = `SELECT count(*) cnt FROM subscribe WHERE chat_id = ${chat.id};`;

		return db.queryAsync(sql);

	}).then(function(result) {
		//console.log(chat.start_time);
		var dateComponents = chat.start_time.match(/^(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2})$/);
		var month = '';
		switch (dateComponents[2]) {
			case '01':
				month = 'января';
				break;
			case '02':
				month = 'февраля';
				break;
			case '03':
				month = 'марта';
				break;
			case '04':
				month = 'апреля';
				break;
			case '05':
				month = 'мая';
				break;
			case '06':
				month = 'июня';
				break;
			case '07':
				month = 'июля';
				break;
			case '08':
				month = 'августа';
				break;
			case '09':
				month = 'сентября';
				break;
			case '10':
				month = 'октября';
				break;
			case '11':
				month = 'ноября';
				break;
			case '12':
				month = 'декабря';
				break;
		}

		var startTimeFormatted = `${dateComponents[1]} ${month} в ${dateComponents[4]}:${dateComponents[5]}`;

		res.render('site/chat_pending.pug', {
			chat: chat,
			subscribers: result[0].cnt,
			start_time_formatted: startTimeFormatted,
			isAuthorized: (req.isAuthorized) ? '1' : '0',
			user_nickname: req.user_nickname,
			is_friend_from_pikabu: req.is_friend_from_pikabu,
			is_superadmin: req.is_superadmin
		});

	}).catch(function(err) {

		logger.error(req, err.message, err.stack);
		res.render('errors/500.pug');

	})
}


function chatStarted(req, res, next, chat, db) {
	//console.log('CHAT STARTED');
	var messages = [];
	var chatInfo = null;
	var user = null;

	if (!req.isAuthorized) {
		chatStartedUnauthorized(req, res, next, chat, db);
		return;
	}

	Promise.resolve().then(function() {

		var sql = `
			SELECT
				id,
				nickname,
				avatar,
				is_friend_from_pikabu
			FROM user
			WHERE id = ${req.user_id};`;

		logger.debug(req, sql);	
		return db.queryAsync(sql);
		
	}).then(function(result) {

		user = result[0];

		var sql = ` SELECT (SELECT count(*)
							FROM message 
							WHERE chat_id = ${chat.id}
							  AND user_id = ${req.user_id}
							  AND service_flag = 1) enter_cnt,
						(SELECT count(*)
							FROM message 
							WHERE chat_id = ${chat.id}
							  AND user_id = ${req.user_id}
							  AND service_flag = 2) block_cnt`;

		logger.debug(req, sql);
		return db.queryAsync(sql);

	}).then(function(result) {

		if (result[0].enter_cnt > 0) {
			res.send('Вы уже находитесь в этом чате');
			throw new Error('exit');
		} else if (result[0].block_cnt > 0) {
			res.send('Вы были заблокированы администратором чата и не сможете больше в него войти.');
			throw new Error('exit');
		}

		var sql = `
			SELECT
				message.id  	 	message_id,
				message.body,
				message.attach,
				DATE_FORMAT(message.date_created, '%H:%i') date_created,
				DATE_FORMAT(message.date_created, '%H:%i') date_updated,
				message.service_flag,
				is_markall,
				user.id 			user_id,
				user.nickname,
				user.avatar
			FROM message
			JOIN user ON user.id = message.user_id
			WHERE message.chat_id = ${chat.id}
			ORDER BY message.date_updated
		`;

		logger.debug(req, sql);

		return db.queryAsync(sql);

	}).then(function(result) {				

		messages = result;

		res.render('site/chat_started.pug', {
			messages: messages,
			chat: chat,
			user: user,
			user_id: req.user_id,
			isAuthorized: (req.isAuthorized) ? '1' : '0',
			user_nickname: req.user_nickname,
			is_friend_from_pikabu: req.is_friend_from_pikabu,
			is_superadmin: req.is_superadmin
		});

	}).catch(function(err) {

		if (err.message === 'exit') return;

		logger.error(req, err.message, err.stack);
		res.render('errors/500.pug');

	})
}


function chatStartedUnauthorized(req, res, next, chat, db) {
	
	connectionPromise().then(function(connection) {

		var sql = `	SELECT COUNT(*) users_cnt 
					FROM message
					WHERE chat_id = ${chat.id}
					  AND service_flag = 1;`;

		logger.debug(req, sql);

		return db.queryAsync(sql);

	}).then(function(result) {

		res.render('site/chat_started_user_not_authorized.pug', {
			chat: chat,
			users_cnt: result[0].users_cnt,
			isAuthorized: false
		});

	}).catch(function(err) {

		logger.error(req, err.message, err.stack);
		res.render('errors/500.pug');

	});
}


function chatFinished(req, res, next, chat, db) {
	//console.log('CHAT FINISHED');
	Promise.resolve().then(function(result) {

		res.render('site/chat_finished.pug', {
			chat: chat,
			isAuthorized: (req.isAuthorized) ? '1' : '0',
			user_nickname: req.user_nickname,
			is_friend_from_pikabu: req.is_friend_from_pikabu,
			is_superadmin: req.is_superadmin
		});

	}).catch(function(err) {

		logger.error(req, err.message, err.stack);
		res.render('errors/500.pug');

	});
}

/*function addChat(chat) {
	
}*/

router.post('/isChatRelativeLinkAvailable', function(req, res, next) {
	if (!req.isAuthorized) {
		res.json({
			status: 'not ok'
		});
		return;
	}
	connectionPromise().then(function(connection) {
		var sql = `
			SELECT count(*) cnt
			FROM chat
			WHERE relative_link = '${req.body.chat_relative_link.replace(/\'/g, '\\\'')}'
			  AND status <> 'finished';
		`;

		return connection.queryAsync(sql);

	}).then(function(result) {

		res.json({
			status: (result[0].cnt === 0) ? 'ok' : 'not ok'
		});

	}).catch(function(err) {

		logger.error(req, err.message, err.stack);
		res.json({
			status: 'not ok'
		});

	});
});


router.post('/ajax_chat_room', function(req, res, next) {
	var db = null;
	var userCurrentChats = [];
	var userSubscribedOnChats = [];

	if (!req.isAuthorized) {
		res.json({
			status: 'not ok'
		});
		return;
	}

	connectionPromise().then(function(connection) {

		db = connection;

		var sql = ` SELECT 
						id,
						NAME,
						relative_link,
						DATE_FORMAT(start_time, '%d.%m.%Y %H:%i') start_time,
						(SELECT COUNT(*) FROM message WHERE chat_id = chat.id AND service_flag = 0) message_cnt,
						status
					FROM chat
					WHERE STATUS = 'started'
					  AND id IN (
						SELECT chat_id
						FROM message
						WHERE service_flag = 3
						AND user_id = ${req.user_id})
					ORDER BY start_time DESC;`;

		logger.debug(req, sql);

		return db.queryAsync(sql);

	}).then(function(result) {

		userCurrentChats = result;

		var sql = ` SELECT 
						id,
						NAME,
						relative_link,
						DATE_FORMAT(start_time, '%d.%m.%Y %H:%i') start_time,
						0 message_cnt,
						status
					FROM chat
					WHERE STATUS = 'pending'
					  AND id IN (
						SELECT chat_id
						FROM subscribe
						WHERE user_id = ${req.user_id})
					ORDER BY start_time;`;

		logger.debug(req, sql);

		return db.queryAsync(sql);

	}).then(function(result) {

		userSubscribedOnChats = result;

		res.json({
			status: 'ok',
			chats: userCurrentChats.concat(userSubscribedOnChats)
		})

	}).catch(function(err) {

		res.json({
			status: 'not ok'
		});

	});
});

router.post('/ajax_subscribe', function(req, res, next) {
	var db = null;
	connectionPromise().then(function(connection) {

		db = connection;

		if (!req.body.chat_id.match(/^\d+$/)) {
			throw new Error(`Parameters validation failed: req.body.chat_id = "${req.body.chat_id}"`);
		}
		
		if (!req.body.user_email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
			throw new Error(`Parameters validation failed: req.body.user_email = "${user.email.length}"`);
		}

		var sql = `SELECT count(*) cnt FROM subscribe WHERE chat_id = ${req.body.chat_id} AND user_email = '${req.body.user_email}';`;

		return db.queryAsync(sql);

	}).then(function(result) {

		if (result[0].cnt > 0) {
			throw new Error('Email exists');
		}

		var userId = ( req.user_id ) ? req.user_id : 'NULL';

		var sql = `INSERT INTO subscribe(
			user_email,
			chat_id,
			user_id,
			date_created
		) VALUES (
			'${req.body.user_email}',
			'${req.body.chat_id}',
			${userId},
			NOW()
		);`;

		return db.queryAsync(sql);

	}).then(function(result) {

		res.json({
			status: 'ok'
		});

	}).catch(function(err) {

		res.json({
			status: 'not ok',
			error: err.message
		});
	});
});

router.post('/', function(req, res, next) {
	if (!req.isAuthorized) {
		res.json({
			status: 'not ok'
		});
		return;
	}

	var db = null;
	var redirectLink = req.body.chat_relative_link;

	Promise.resolve().then(function() {
		
		validateChat(req.body);
		util.formatObjectForSQL(req.body);
	
		return connectionPromise();
	
	}).then(function(connection) {

		db = connection;
		var firstMessage = null;
		var chatStartTime = null;
		var chatStatus = null;

		if (req.body.chat_start_time !== 'NULL') {
			chatStartTime = `STR_TO_DATE(${req.body.chat_start_time}, '%d.%c.%Y %H:%i')`;
			chatStatus = '\'pending\'';
		} else {
			chatStartTime = 'NOW()';
			chatStatus = '\'started\'';
		}

		var sql = `
			INSERT INTO chat(
				name,
				relative_link,
				start_time,
				status,
				date_created,
				date_updated,
				owner_id,
				first_message
			)
			VALUES (
				${req.body.chat_name},
				${req.body.chat_relative_link},
				${chatStartTime},
				${chatStatus},
				NOW(),
				NOW(),
				${req.user_id},
				${req.body.chat_first_message}
			);
		`;

		logger.debug(req, sql);

		return db.queryAsync(sql);
	
	}).then(function(result) {

		logger.debug(req, result);

		var promiseChain = Promise.resolve().then(function() {
			var sql = ` INSERT INTO message (
							body,
							date_created,
							date_updated,
							user_id,
							chat_id,
							service_flag)
						VALUES (
							'Чтобы пригласить друзей в чат просто киньте им ссылку на него',
							NOW(),
							NOW(),
							-1,
							${result.insertId},
							-1
						)`;

			return db.queryAsync(sql);

		});

		if (req.body.chat_first_message !== 'NULL') {

			promiseChain = promiseChain.then(function() {

				var sql = ` INSERT INTO message (
								body,
								date_created,
								date_updated,
								user_id,
								chat_id,
								service_flag)
							VALUES (
								${req.body.chat_first_message},
								NOW(),
								NOW(),
								${req.user_id},
								${result.insertId},
								0
							)`;

				logger.debug(req, sql);

				return db.queryAsync(sql);

			})
		}

		return promiseChain;
	
	}).then(function(result) {

		if (result) {
			logger.debug(req, result);
		}

		res.json({
			status: 'ok',
			shouldRedirect: (req.body.chat_start_time === 'NULL') ? true : false,
			redirectLink: redirectLink
		});

	}).catch(function(err) {

		logger.error(req, err.message, err.stack);
		res.json({
			status: 'not ok'
		});

	});
});

module.exports = router;