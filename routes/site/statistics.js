'use strict';

const router = require('express').Router();
const logger = require('log4js').getLogger();
const Promise = require('bluebird');
const os = require('os');
const connectionPromise = require('../../components/connectionPromise.js');
const servers = require('../../components/servers.js');
const myLogger = require('../../components/myLogger.js');

router.get('/ajax_stat_sockets', function(req, res, next) {
	var db = null;
	var countRoomsTotal = 0;
	var countRoomsReal = 0;
	var countSocketsConnected = 0;

	var isAuthorized = false;

	if (req.isAuthorized & req.is_superadmin) isAuthorized = true;
	if (req.cookies['AlaskaChatStatisticsAdminToken'] === 'BQapf82W2C38RrD6') isAuthorized = true;

	if (!isAuthorized) {
		res.json({
			status: 'not ok',
			message: 'Permission denied. You do not have superadmin rights.'});
		return;
	}

	connectionPromise().then(function(connection) {
		
		db = connection;

		for (var room_id in servers.io.sockets.adapter.rooms) {
			if (room_id.match(/^\d+$/)) {
				countRoomsReal++;
			}
			countRoomsTotal++;
		}

		for (var sock_id in servers.io.sockets.connected) {
			countSocketsConnected++;
		}

		//console.log('servers.io.sockets.adapter.rooms=', servers.io.sockets.adapter.rooms);
		//console.log('servers.io.sockets.connected=', servers.io.sockets.connected);

	}).then(function() {

		res.json({
			status: 'ok',
			countRoomsTotal: countRoomsTotal,
			countRoomsReal: countRoomsReal,
			countSocketsConnected: countSocketsConnected
		});
		
	}).catch(function(err) {

		logger.error(err.message, err.stack);

		res.json({
			status: 'not ok',
			message: err.message
		});
	});
});

router.get('/ajax_stat_memory', function(req, res, next) {
	
	if (req.cookies['AlaskaChatStatisticsAdminToken'] !== 'BQapf82W2C38RrD6') {
		res.json({
			status: 'not ok',
			message: 'У вас нет необходимого куки'
		})
		return;
	}

	Promise.resolve().then(function() {

		res.json({
			status: 'ok',
			freeMem: os.freemem(),
			totalMem: os.totalmem(),
			loadAvg: os.loadavg()
		});
		
	}).catch(function(err) {

		logger.error(err.message, err.stack);

		res.json({
			status: 'not ok'
		});
	});
});

router.get('/ajax_stat_users', function(req, res, next) {

	if (!req.isAuthorized || !req.is_superadmin) {
		res.json({
			status: 'not ok',
			message: 'Permission denied. You do not have superadmin rights.'});
		return;
	}

	connectionPromise().then(function(connection) {

		var sql = `SELECT
					(SELECT COUNT(*) 
					FROM v_user) users_total,
					 (SELECT COUNT(*)
					 FROM v_user
					 JOIN session ON v_user.user_id = session.user_id) users_online,
					(SELECT COUNT(*)
					FROM v_user
					WHERE TIMESTAMPDIFF(HOUR, user_date_register, NOW()) < 24) new_users_last_day,
					(SELECT COUNT(*)
					FROM v_user
					WHERE TIMESTAMPDIFF(HOUR, user_date_register, NOW()) < 24 * 7) new_users_last_week,
					(SELECT COUNT(*)
					FROM v_user
					WHERE TIMESTAMPDIFF(HOUR, user_date_register, NOW()) < 24 * 14) new_users_last_two_weeks`;

		return connection.queryAsync(sql);

	}).then(function(result) {

		res.json({
			status: 'ok',
			data: result[0]
		});

	}).catch(function(err) {

		myLogger.error(req, err.message, err.stack);
		res.json({
			status: 'not ok',
			message: err.message
		});
	});

});

router.get('/', function(req, res, next) {
		
	if (!req.isAuthorized) {
		res.redirect('/?message=Пожалуйста авторизуйтесь на сайте');
		return;
	}

	if (!req.is_superadmin) {
		res.render('errors/404.pug');
		return;
	}
	
	res.render('site/statistics.pug', {
		isAuthorized: (req.isAuthorized) ? '1' : '0',
		user_nickname: req.user_nickname,
		is_superadmin: req.is_superadmin
	});
});

module.exports = router;